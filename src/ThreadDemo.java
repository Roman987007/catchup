/**
 * * В классе {@code ThreadDemo} производится вызов два анонимных дочерних класса,
 * которые выводят сообщения для первого класса "1", а для второго "2" с целью
 * устраивания догонялок между двумя потоками.
 *
 * @author Петрушов Роман.
 * @since 18.11.2016
 */
public class ThreadDemo {
    public static void main(String[] args) {
        new NewThread("first", 1, 999, 0, 100);
        new NewThread("second", 10, 999, 0, 100);
    }
}
