/**
 * Демонстрационный класс {@code ThreadDemo}, который демонстрирует динамическое изменение приоритета потока
 * с целью устраивания догонялок между двумя потоками.
 *
 * @author Петрушов Роман.
 * @since 18.11.2016
 */
public class NewThread implements Runnable {
    Thread thread;
    private String message;
    private int start;
    private int finish;
    private int provision;

    public NewThread() {
        thread = new Thread(this, "Новый поток");
        thread.setPriority(Thread.NORM_PRIORITY);
        this.message = "Новый поток";
        this.start = 600;
        this.finish = 1;
        this.provision = 100;
        thread.start();
    }

    public NewThread(String message, int priority, int start, int finish, int provision) {
        thread = new Thread(this, message);
        thread.setPriority(priority);
        this.message = message;
        this.start = start;
        this.finish = finish;
        this.provision = provision;
        thread.start();
    }

    /**
     * Поток выводит счётчик {@code starting - finishing} раз
     */
    @Override
    public void run() {
        System.out.println(message + " поток запущен");
        for (int i = start; i > finish; i--) {
            changePriority(i);
            System.out.println(message + " " + i);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                System.out.println(message + " поток прерван");
            }
        }
        System.out.println(message + " поток завершен");
    }

    /**
     * Метод меняет приоритет потока
     *
     * В зависимости от заданного условия и приоритета потока
     * на данный момент, приоритет меняется на минимальный, если
     * приоритет больше пяти, и на максимальный, если меньше
     *
     * @param i - счётчик цикла
     */
    private void changePriority(int i) {
        if ((i % provision) == 0) {
            if (thread.getPriority() > 5) {
                thread.setPriority(Thread.MIN_PRIORITY);
            } else {
                thread.setPriority(Thread.MAX_PRIORITY);
            }
        }
    }
}